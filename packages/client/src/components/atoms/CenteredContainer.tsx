import * as colors from '@nkchat/client/src/colors';
import styled from 'styled-components';

export default styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
  background: ${colors.primary};
`;
