import * as colors from '@nkchat/client/src/colors';
import styled from 'styled-components';

export default styled.div`
  border-radius: 2px;
  padding: 15px;
  margin-bottom: 20px;
  background: ${colors.error};
  color: #fff;
`;
